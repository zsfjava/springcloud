package org.simple.plan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 收藏作品关联表
 */
@Data
@TableName("design_star")
@EqualsAndHashCode(callSuper = true)
public class DesignStar extends Model<DesignStar> {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @NotNull(message = "主键id不能为空")
    private String id;
    /**
     * 关联作品id
     */
    private String opusid;
    /**
     * 关联用户id
     */
    private String userid;
    /**
     * 状态
     */
    private String status;
    /**
     * 收藏时间
     */
    private LocalDateTime createdate;

}
