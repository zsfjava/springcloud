package org.simple.plan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.simple.plan.entity.DesignUser;

/**
 * 用户信息表
 */
public interface DesignUserMapper extends BaseMapper<DesignUser> {

}
