package org.simple.plan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.simple.plan.entity.DesignOpus;
import org.simple.plan.mapper.DesignOpusMapper;
import org.simple.plan.service.DesignOpusService;
import org.springframework.stereotype.Service;

/**
 * 作品设计表
 */
@Service
public class DesignOpusServiceImpl extends ServiceImpl<DesignOpusMapper, DesignOpus> implements DesignOpusService {

}
