package org.simple.plan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.simple.plan.entity.DesignSort;
import org.simple.plan.mapper.DesignSortMapper;
import org.simple.plan.service.DesignSortService;
import org.springframework.stereotype.Service;

/**
 * 分类表
 */
@Service
public class DesignSortServiceImpl extends ServiceImpl<DesignSortMapper, DesignSort> implements DesignSortService {

}
