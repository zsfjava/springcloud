package org.simple.plan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.simple.plan.entity.DesignUser;
import org.simple.plan.mapper.DesignUserMapper;
import org.simple.plan.service.DesignUserService;
import org.springframework.stereotype.Service;

/**
 * 用户信息表
 */
@Service
public class DesignUserServiceImpl extends ServiceImpl<DesignUserMapper, DesignUser> implements DesignUserService {

}
