package org.simple.plan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.simple.plan.dto.StarOpusDto;
import org.simple.plan.entity.DesignStar;
import org.simple.plan.mapper.DesignStarMapper;
import org.simple.plan.service.DesignStarService;
import org.simple.security.utils.AuthUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 收藏作品关联表
 */
@Service
public class DesignStarServiceImpl extends ServiceImpl<DesignStarMapper, DesignStar> implements DesignStarService {

	@Override
	public List<StarOpusDto> queryStarList() {
		return baseMapper.queryStarList(AuthUtils.getUser().getId());
	}
}
