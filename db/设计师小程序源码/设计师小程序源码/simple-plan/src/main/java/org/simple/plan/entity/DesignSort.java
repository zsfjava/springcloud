package org.simple.plan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 分类表
 */
@Data
@TableName("design_sort")
@EqualsAndHashCode(callSuper = true)
public class DesignSort extends Model<DesignSort> {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @NotNull(message = "主键id不能为空")
    private String id;
    /**
     * 关联用户id
     */
    private String userid;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 创建时间
     */
    private LocalDateTime createdate;

}
