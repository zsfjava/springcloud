package org.simple.plan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.plan.entity.DesignOpus;

/**
 * 作品设计表
 */
public interface DesignOpusService extends IService<DesignOpus> {

}
