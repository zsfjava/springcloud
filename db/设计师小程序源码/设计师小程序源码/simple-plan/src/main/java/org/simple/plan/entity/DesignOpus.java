/**
 * All rights reserved, Designed By xiangyouyukeji
 */
package org.simple.plan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 作品设计表
 */
@Data
@TableName("design_opus")
@EqualsAndHashCode(callSuper = true)
public class DesignOpus extends Model<DesignOpus> {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @NotNull(message = "主键id不能为空")
    private String id;
    /**
     * 标题
     */
    private String name;
    /**
     * 查看次数
     */
    private String look;
    /**
     * 收藏人数
     */
    private String heart;
    /**
     * 标签
     */
    private String tags;
    /**
     * 描述
     */
    private String remark;
    /**
     * 图片，最多10张
     */
    private String pics;
    /**
     * 图片标题
     */
    private String titles;
    /**
     * 地区
     */
    private String area;
    /**
     * 面积
     */
    private String sizes;
    /**
     * 户型
     */
    private String houseType;
    /**
     * 造价
     */
    private String price;
    /**
     * 关联用户id
     */
    private String userid;
    /**
     * 关联分类
     */
    private String sort;
    /**
     * 修改时间
     */
    private LocalDateTime updatedate;
	/**
	 * 发布状态
	 */
	private String status;

}
