
package org.simple.plan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.simple.plan.dto.StarOpusDto;
import org.simple.plan.entity.DesignStar;

import java.util.List;

/**
 * 收藏作品关联表
 */
public interface DesignStarMapper extends BaseMapper<DesignStar> {

	List<StarOpusDto> queryStarList(@Param("userid")String userid);

}
