package org.simple.plan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.simple.plan.entity.DesignSort;

/**
 * 分类表
 */
public interface DesignSortMapper extends BaseMapper<DesignSort> {

}
