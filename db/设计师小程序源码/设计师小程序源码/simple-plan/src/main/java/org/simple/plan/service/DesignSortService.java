package org.simple.plan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.plan.entity.DesignSort;

/**
 * 分类表
 */
public interface DesignSortService extends IService<DesignSort> {

}
