package org.simple.plan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.plan.entity.DesignUser;

/**
 * 用户信息表
 */
public interface DesignUserService extends IService<DesignUser> {

}
