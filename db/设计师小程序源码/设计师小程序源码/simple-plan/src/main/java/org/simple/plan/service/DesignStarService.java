package org.simple.plan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.plan.dto.StarOpusDto;
import org.simple.plan.entity.DesignStar;

import java.util.List;

/**
 * 收藏作品关联表
 */
public interface DesignStarService extends IService<DesignStar> {

	List<StarOpusDto> queryStarList();
}
