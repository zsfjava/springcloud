package org.simple.plan.dto;

import lombok.Data;
import org.simple.plan.entity.DesignStar;

@Data
public class StarOpusDto extends DesignStar {

	/**
	 * 作品图片
	 * */
	private final String pics;

	/**
	 * 作品标题
	 * */
	private final String title;

	/**
	 * 关联设计师
	 * */
	private final String author;

	/**
	 * 户型结构
	 * */
	private final String houseType;

	/**
	 * 面积
	 * */
	private final String sizes;

	/**
	 * 造价
	 * */
	private final String price;

	/**
	 * 地区
	 * */
	private final String area;
}
