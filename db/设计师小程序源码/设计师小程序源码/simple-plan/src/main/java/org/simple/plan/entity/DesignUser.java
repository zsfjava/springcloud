package org.simple.plan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 用户信息表
 */
@Data
@TableName("design_user")
@EqualsAndHashCode(callSuper = true)
public class DesignUser extends Model<DesignUser> {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @NotNull(message = "主键id不能为空")
    private String id;
    /**
     * 昵称
     */
    private String name;
    /**
     * 用户类型；01：企业 02:个人
     */
    private String type;
    /**
     * 从业年限
     */
    private String work;
    /**
     * 服务地区
     */
    private String serviceArea;
    /**
     * 标签
     */
    private String tags;
    /**
     * 简介
     */
    private String remark;
    /**
     * 微信号
     */
    private String wx;
    /**
     * 微信userid
     */
    private String openid;
    /**
     * 关联用户id
     */
    private String userid;
    /**
     * 创建时间
     */
    private LocalDateTime createdate;
    /**
     * 服务报价
     */
    private String offer;
    /**
     * 发布状态
     */
    private String status;

	/**
	 * 微信头像
	 * */
	private String avatar;

	/**
	 * 对外头像
	 * */
	private String outavatar;


	/**
	 * 微信全平台唯一用户id
	 * */
	private String uniopenid;

	/**
	 * 手机号
	 * */
	private String phone;

	/**
	 * 热度
	 * */
	private String heart;


}
