package org.simple.plan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.simple.plan.entity.DesignOpus;

/**
 * 作品设计表
 */
public interface DesignOpusMapper extends BaseMapper<DesignOpus> {

}
