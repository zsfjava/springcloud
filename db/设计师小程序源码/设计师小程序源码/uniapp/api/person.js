import request from '@/utils/request'

export function getCataPage(id) {
    return request.get({
        url: '/plan/designsort/page',
        method: 'get'
    })
}

export function addCataObj(data) {
	return request.post('/plan/designsort/add',data)
}

export function delCataObj(params) {
    return request.get({
        url: '/plan/designsort/delete',
        method: 'get',
		params:params
    })
}

export function decryptPhone(params) {
    return request.get({
        url: '/plan/designuser/decrypt',
        method: 'get',
		params:params
    })
}

export function editUserInfo(data) {
	return request.post('/plan/designuser/edit1',data)
}

export function getStarList() {
    return request.get({
        url: '/plan/star/list',
        method: 'get'
    })
}

export function getOpusList(params) {
    return request.get({
        url: '/plan/opus/page',
        method: 'get',
		params:params
    })
}

export function addOpus(data) {
	return request.post('/plan/opus/add',data)
}

export function editOpus(data) {
	return request.post('/plan/opus/edit',data)
}

export function getOpus(id) {
    return request.get({
        url: '/plan/opus/'+id,
        method: 'get'
    })
}

export function openOpus(id) {
    return request.get({
        url: '/plan/opus/open/'+id,
        method: 'get'
    })
}
export function closeOpus(id) {
    return request.get({
        url: '/plan/opus/close/'+id,
        method: 'get'
    })
}

export function addStar(id) {
    return request.get({
        url: '/plan/star/add/'+id,
        method: 'get'
    })
}
export function delStar(id) {
    return request.get({
        url: '/plan/star/del/'+id,
        method: 'get'
    })
}