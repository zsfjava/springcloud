import request from '@/utils/request'

const secret = "6ceb70f6cca98475ae91bb8aa9657b6d"
//账户密码登陆
export const loginByThirdParty = (data) => {
  return request.post({
    url: '/auth/oauth/token',
    method: 'post',
    params: data,
    headers: {
      isAuth: false,
      sp: secret,
      'grant_type': 'third_code'
    }
  })
}

export function getUserInfo() {
	return request.get({
		url: '/plan/designuser/getUser',
		method: 'get',
	})
}

export function logout() {
  return request({
    url: '/auth/sp/logout',
    method: 'get'
  })
}
