import request from '@/utils/request'

export function queryDic(code) {
    return request.get({
        url: '/center/dict/vals/' + code,
        method: 'get',
    })
}