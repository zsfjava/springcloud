import request from '@/utils/request'


/**
 * 获取首页热门推荐列表
 */
export function getMainInfo(data){
	return request.get({
		url: '/plan/designuser/page',
		headers: {
			isToken: false
		},
		method: 'get',
		params:data
	})
}

export function queryUserInfo(id) {
    return request.get({
        url: '/plan/designuser/api/'+id,
		headers: {
			isToken: false
		},
        method: 'get'
    })
}

export function queryCataList(userid) {
    return request.get({
        url: '/plan/designsort/api/'+userid,
		headers: {
			isToken: false
		},
        method: 'get'
    })
}

export function queryOpusInfo(id) {
    return request.get({
        url: '/plan/opus/api/'+id,
		headers: {
			isToken: false
		},
        method: 'get'
    })
}

export function getOpusList(data){
	return request.get({
		url: '/plan/opus/api/page',
		headers: {
			isToken: false
		},
		method: 'get',
		params:data
	})
}

export function addLookOpus(id){
	return request.get({
		url: '/plan/opus/api/look/'+id,
		headers: {
			isToken: false
		},
		method: 'get'
	})
}

export function getWechatList(){
	return request.get({
		url: '/plan/planshop/api/tool', 
		headers: {
			isToken: false
		},
		method: 'get'
	})
}