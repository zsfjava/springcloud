import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
//import hljs from 'highlight.js'
import {_router} from '@/utils/router.js'
//import 'highlight.js/styles/default.css' // 这里可以切换不同的主题
Vue.config.productionTip = false
 Vue.prototype.$prouter = _router
App.mpType = 'app'
Vue.use(uView)
// Vue.directive('highlight',function (el) {
//     let blocks = el.querySelectorAll('pre code');
//     setTimeout(() =>{
//         blocks.forEach((block)=>{
// 			hljs.highlightBlock(block)
//         })
//     }, 200)
// })
const app = new Vue({
    ...App
})
app.$mount()





