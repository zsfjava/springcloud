import Fly from 'flyio/dist/npm/wx'
import {
	VUE_APP_API_URL,sessionAttr
} from '@/config'
let service = new Fly();
service.config.baseURL = VUE_APP_API_URL
service.config.timeout = 10000;

/**
 * 请求拦截器
 */
service.interceptors.request.use(
	(request) => {
		const isToken = (request.headers || {}).isToken === false
		const token = uni.getStorageSync(sessionAttr).access_token
		if (token && !isToken) {
			request.headers["Authorization"] = 'Bearer ' + token;
		}
		if (request.method === "GET") {
			//request.body = qs.stringify(request.body, { arrayFormat: "repeat" });
		}
		if (request.method == 'POST' || request.method == 'PUT' || request.method == 'DELETE') {
			// if(request.isFile){
			// 	request.headers["Content-Type"] = 'multipart/form-data';
			// 	request.data = request.body
			// 	console.log('请求对象：',request)
			// }
		}
		return request;
	},
	(error) => {
		return Promise.reject(error);
	}
);

/**
 * 请求响应拦截器
 */
service.interceptors.response.use(
	(res) => {
		const data = res.data || {}


		if (res.status !== 200) {
			return Promise.reject({
				msg: '请求失败',
				res,
				data
			})
		}
		if ([401, 403].indexOf(res.status) !== -1) {
			uni.setStorageSync(sessionAttr, {})
			uni.switchTab({
				url: '/pages/personal/index'
			})
			//return Promise.reject({ msg: res.data.msg, res, data, toLogin: true })
		} else if (res.status === 200) {
			return Promise.resolve(data, res)
		} else if (res.status == 5101) {
			return Promise.reject({
				msg: res.data.msg,
				res,
				data
			})
		} else {
			return Promise.reject({
				msg: res.data.msg,
				res,
				data
			})
		}
		return response;
	},
	(error) => {
		const response = error.response;
        var pages = getCurrentPages() // 获取栈实例
        let currentRoute  = pages[pages.length-1].route;
		if (response.status && response.status === 401) {
			if ('pages/personal/index' == currentRoute) {
				return Promise.reject(response);
			}
			uni.showToast({
				title: '登录失效，请重新登录！',
				icon: 'none',
				duration: 2000
			})
			uni.setStorageSync(sessionAttr, {})
			uni.switchTab({
				url: '/pages/personal/index'
			})
		} else if (response.status && response.status === 403) {
			uni.showToast({
				title: '对不起，无权操作！',
				icon: 'none',
				duration: 2000
			})
		} else {
			if (response.data && response.data.msg) {
				uni.showToast({
					title: response.data.msg,
					icon: 'none',
					duration: 2000,
				})
			} else {
				uni.showToast({
					title: '系统错误，请尝试重新操作！',
					icon: 'none',
					duration: 2000
				})
			}
		}


		return Promise.reject(response);
	}
);

export default service;
