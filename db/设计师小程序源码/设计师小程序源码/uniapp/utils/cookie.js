export function getSyncV(key) {
    return uni.getStorageSync(key)
}

export function getV(key) {
    return uni.getStorage(key)
}

export function setV(key,value) {
     uni.setStorage({
		key:key,
		data:value
	})
}

export function setSyncV(key,value) {
     uni.setStorageSync({
		key:key,
		data:value
	})
}
