const whiteList = ['pages/home/index', 'pages/login/index']

const isLogin = () => {
	return !uni.getStorageSync("pengpengyu_token")
}

const navigateTo = (params) => {
	if (isLogin()) {
		if (whiteList.includes(params.path)) {
			uni.navigateTo(params)
		} else {
			uni.navigateTo('/pages/login/index')
		}
	} else {
		uni.navigateTo(params)
	}
}

const switchTab = (params) => {
	if (isLogin()) {
		if (whiteList.includes(params.path)) {
			uni.switchTab(params)
		} else {
			uni.navigateTo('/pages/login/index')
		}
	} else {
		uni.switchTab(params)
	}
}


const _router = {
	navigateTo,
	switchTab
}

export  {
	_router,whiteList
}
