/*
 Navicat Premium Data Transfer

 Source Server         : 仿真数据库
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : sh-cdb-8n8zfbbc.sql.tencentcdb.com:59296
 Source Schema         : simple_plan

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 14/08/2022 14:26:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for design_opus
-- ----------------------------
DROP TABLE IF EXISTS `design_opus`;
CREATE TABLE `design_opus` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `look` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '查看次数',
  `heart` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收藏人数',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签',
  `remark` varchar(1000) DEFAULT NULL COMMENT '描述',
  `pics` text COMMENT '图片，最多10张',
  `titles` varchar(1000) DEFAULT NULL COMMENT '图片标题',
  `area` varchar(255) DEFAULT NULL COMMENT '地区',
  `sizes` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '面积',
  `house_type` varchar(24) DEFAULT NULL COMMENT '户型',
  `price` varchar(20) DEFAULT NULL COMMENT '造价',
  `userid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关联用户id',
  `sort` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关联分类',
  `updatedate` datetime DEFAULT NULL COMMENT '修改时间',
  `status` varchar(2) DEFAULT NULL COMMENT '发布状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作品设计表';

-- ----------------------------
-- Table structure for design_sort
-- ----------------------------
DROP TABLE IF EXISTS `design_sort`;
CREATE TABLE `design_sort` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `userid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联用户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `createdate` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Table structure for design_star
-- ----------------------------
DROP TABLE IF EXISTS `design_star`;
CREATE TABLE `design_star` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `opusid` varchar(64) DEFAULT NULL COMMENT '关联作品id',
  `userid` varchar(64) DEFAULT NULL COMMENT '关联用户id',
  `status` varchar(2) DEFAULT NULL COMMENT '状态',
  `createdate` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏作品关联表';

-- ----------------------------
-- Table structure for design_user
-- ----------------------------
DROP TABLE IF EXISTS `design_user`;
CREATE TABLE `design_user` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户类型；01：企业 02:个人',
  `work` varchar(20) DEFAULT NULL COMMENT '从业年限',
  `service_area` varchar(32) DEFAULT NULL COMMENT '服务地区',
  `tags` varchar(255) DEFAULT NULL COMMENT '标签',
  `remark` varchar(255) DEFAULT NULL COMMENT '简介',
  `wx` varchar(64) DEFAULT NULL COMMENT '微信号',
  `openid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '微信userid',
  `userid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关联用户id',
  `createdate` datetime DEFAULT NULL COMMENT '创建时间',
  `offer` varchar(255) DEFAULT NULL COMMENT '服务报价',
  `status` varchar(2) DEFAULT NULL COMMENT '发布状态',
  `uniopenid` varchar(64) DEFAULT NULL COMMENT '唯一openid',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `outavatar` varchar(255) DEFAULT NULL COMMENT '对外头像',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `heart` varchar(100) DEFAULT NULL COMMENT '热度：作品收藏数量+作品浏览数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

SET FOREIGN_KEY_CHECKS = 1;
